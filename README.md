# turbulator airfoil

copied from example https://www.youtube.com/watch?v=9uJBzKoAqxE 
and its code https://github.com/openfoamtutorials/single_bend_airfoil.git

Estimate the performance of simple single-bend airfoils.

*  run gmshgenerator notebook to generate the turbulator airfoil



* convert to mesh 
 !!!  you will need to edit out comments in foil2.geo and make the variable names match up !!!
    ~/executables/gmsh-3.0.4-Linux/bin/gmsh -3 -o main.msh foil2.geo 


  !!! case is the directory with the workings in it
  !!! run the clean.sh script to remove all the workings, except the 0 starting position
  
* gmshToFoam main.msh -case case

* changeDictionary -case case

* simpleFoam -case case

* run paraview and Open "view.foam" and/or view force coefficients in ./case/postProcessing/.
   [] click apply to make the image show up
   []tick Camera Parallel Projection

** changes to input data case/0/U
internalField   uniform (90 0 0);  # changed from 9 to 90 to see if faster flow causes more visible turbulance
